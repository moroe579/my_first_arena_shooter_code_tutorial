{
    "id": "b99dc199-1ec4-4be0-a248-77b0d18cd39d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score",
    "eventList": [
        {
            "id": "c53907f9-0e71-45bd-a6eb-c800e26c024a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b99dc199-1ec4-4be0-a248-77b0d18cd39d"
        },
        {
            "id": "7fe0cd07-13df-49fb-848f-f9999f07fa99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b99dc199-1ec4-4be0-a248-77b0d18cd39d"
        },
        {
            "id": "3535a546-bd95-4550-bcb1-e95944b3e29c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b99dc199-1ec4-4be0-a248-77b0d18cd39d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}