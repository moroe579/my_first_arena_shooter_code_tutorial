{
    "id": "bc627c33-da6d-489a-8002-d87d78ff6878",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemy_death",
    "eventList": [
        {
            "id": "d2738cba-102d-4fee-bc99-2d53b95d687b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc627c33-da6d-489a-8002-d87d78ff6878"
        },
        {
            "id": "ecdc4e72-d4ba-4689-847b-f21f3af42863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc627c33-da6d-489a-8002-d87d78ff6878"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85a7b1f6-c244-4392-aff4-152741007680",
    "visible": true
}