{
    "id": "cbd96c47-2897-4c77-aedb-4e7571575f43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "17ee91bb-db9c-4589-848b-7756df850010",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbd96c47-2897-4c77-aedb-4e7571575f43"
        },
        {
            "id": "fefeeda7-7ba2-4faa-b926-9cfeaceb5d8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cbd96c47-2897-4c77-aedb-4e7571575f43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dac2beab-2c0e-45c5-8ed2-5b6dcfdd2174",
    "visible": true
}