{
    "id": "9f2f206e-fc90-42b6-b14e-ac3f590021a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "7ff4462b-3dbe-41f5-9702-4cd0a371b563",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f2f206e-fc90-42b6-b14e-ac3f590021a2"
        },
        {
            "id": "4b95c15e-84ea-4134-bf92-aa0894fbca51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0855091c-e921-4e84-9a08-f981314692d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9f2f206e-fc90-42b6-b14e-ac3f590021a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e75233f-b45f-491c-98b4-7852bebc7028",
    "visible": true
}