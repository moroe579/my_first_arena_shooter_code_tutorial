{
    "id": "2d36c6d9-aee2-4f37-ad32-292347d24a8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quit",
    "eventList": [
        {
            "id": "e34293b2-bb3d-41d7-bd69-62851b1d9961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2d36c6d9-aee2-4f37-ad32-292347d24a8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8735e6a3-bac4-41cc-afec-685add6961ed",
    "visible": true
}