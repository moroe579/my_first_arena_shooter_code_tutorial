{
    "id": "36c11079-f38b-4949-ba0a-431b631dcd2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyspawn",
    "eventList": [
        {
            "id": "db590c44-f9c9-427d-af1b-f04c0752c9ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36c11079-f38b-4949-ba0a-431b631dcd2f"
        },
        {
            "id": "2f7826ef-add0-4373-80a9-ce69208d6451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36c11079-f38b-4949-ba0a-431b631dcd2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8dc2ce79-2f08-43ef-92fd-74fda8fa6ac2",
    "visible": true
}