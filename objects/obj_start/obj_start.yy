{
    "id": "6cd4c4e5-8c15-4a07-a48a-18438f0a3db6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start",
    "eventList": [
        {
            "id": "fa2991a2-3565-49fb-9ef1-964f5ab47f0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6cd4c4e5-8c15-4a07-a48a-18438f0a3db6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d8ad1d2-f9a2-4115-8073-667eaa8bb768",
    "visible": true
}