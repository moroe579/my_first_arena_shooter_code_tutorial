{
    "id": "0d95221c-4561-4746-a16d-7fe8720bfcae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "c61fec8b-c405-45c2-b09c-311f592572b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        },
        {
            "id": "10679895-a166-40fc-bf11-c2664f770132",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        },
        {
            "id": "634393bc-617a-4ae6-9bcb-f3e453650038",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0855091c-e921-4e84-9a08-f981314692d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        },
        {
            "id": "ac5a8ecb-a2ea-4304-8abe-5e90d452c697",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "26a0074e-7033-44c2-8bbc-adc41299a5b2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        },
        {
            "id": "1d249117-f1d6-48e5-89fa-335099d860a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        },
        {
            "id": "f0a18a4a-95bb-49c9-ba24-e4b9558c2d9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "49c18ca6-0ee4-4c6a-bafa-d1a3beff7807",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0d95221c-4561-4746-a16d-7fe8720bfcae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c160b207-9e4e-49ac-8e19-b8f286b05bdd",
    "visible": true
}