//Movement
if (keyboard_check(ord("D"))) x += 4;
if (keyboard_check(ord("A"))) x -= 4;
if (keyboard_check(ord("S"))) y += 4;
if (keyboard_check(ord("W"))) y -= 4;

image_angle = point_direction(x, y, mouse_x, mouse_y);

//Shooting
if (defaultgun) = 1
{
	if (mouse_check_button(mb_left)) && (cooldown < 1)
		{
			 instance_create_layer(x, y, "bullet_layer", obj_bullet);
			cooldown = 10;
			audio_play_sound(snd_gunshot, 1, false);
		}
	cooldown = cooldown - 1;
}

if (machinegun) = 1
{
	if (mouse_check_button(mb_left)) && (cooldown < 1)
		{
			instance_create_layer(x, y, "bullet_layer", obj_bullet);
			cooldown = 3;
			audio_play_sound(RAAAAAAAAAAA, 0, false);
		}
	cooldown = cooldown - 1;
	
}
	

