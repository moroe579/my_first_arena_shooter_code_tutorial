if (instance_exists(obj_player))
{
move_towards_point(obj_player.x,obj_player.y, spd);
}
powerup = random_range(1, 10);
image_angle = direction;

if (hp) <= 0
{
	if (powerup) >= 9
	{
		instance_create_layer(x, y, "Instances", obj_powerup)
	}

	with(obj_score) thescore = thescore + 5;
	audio_sound_pitch(snd_death, random_range(0.8, 1.2));
	audio_play_sound(snd_death, 0, false);
	instance_create_layer(x, y, "Enemy_Splat", enemy_death);
	instance_destroy();
}