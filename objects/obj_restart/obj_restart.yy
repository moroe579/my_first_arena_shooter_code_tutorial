{
    "id": "45d92cd3-b606-4b56-b076-4c9381ba559e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_restart",
    "eventList": [
        {
            "id": "5da45d26-7283-4dd8-8fd6-c336fe5dd415",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "45d92cd3-b606-4b56-b076-4c9381ba559e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b0c57bbd-0ece-480d-ad7f-2533e573c31d",
    "visible": true
}