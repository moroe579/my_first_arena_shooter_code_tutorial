{
    "id": "26a0074e-7033-44c2-8bbc-adc41299a5b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powerup",
    "eventList": [
        {
            "id": "9986a4a0-1bad-4554-aa24-9f1f5eabb03f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26a0074e-7033-44c2-8bbc-adc41299a5b2"
        },
        {
            "id": "1d84e909-5425-4fa8-bc51-48794ee0587f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "26a0074e-7033-44c2-8bbc-adc41299a5b2"
        },
        {
            "id": "f52ba101-f053-43f8-87e8-5cc66cf719e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0d95221c-4561-4746-a16d-7fe8720bfcae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "26a0074e-7033-44c2-8bbc-adc41299a5b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "539e9093-a92c-4da0-bbb9-3b3a43e79876",
    "visible": true
}