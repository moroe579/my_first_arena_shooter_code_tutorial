{
    "id": "9d257858-05c5-4566-b87b-f0101322f4c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darktile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b6fc08a-f123-4b17-a37b-6436a78a8e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d257858-05c5-4566-b87b-f0101322f4c8",
            "compositeImage": {
                "id": "6455343a-bad4-4139-9b4f-43a6106aee9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b6fc08a-f123-4b17-a37b-6436a78a8e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476ed51d-d972-4b19-b71a-26a73b362b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b6fc08a-f123-4b17-a37b-6436a78a8e87",
                    "LayerId": "336903f8-689a-494f-9b5e-3f38e0ce8747"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "336903f8-689a-494f-9b5e-3f38e0ce8747",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d257858-05c5-4566-b87b-f0101322f4c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}