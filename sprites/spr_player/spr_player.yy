{
    "id": "c160b207-9e4e-49ac-8e19-b8f286b05bdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 23,
    "bbox_right": 90,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d31f966e-dbdb-484d-8716-25c946e52e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c160b207-9e4e-49ac-8e19-b8f286b05bdd",
            "compositeImage": {
                "id": "493bd4d6-921c-4f8d-a4d4-cdc9f102b811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31f966e-dbdb-484d-8716-25c946e52e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf77e38-c871-4491-9e90-6d616ff39feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31f966e-dbdb-484d-8716-25c946e52e40",
                    "LayerId": "2c3c54c3-eb86-46c3-98ca-b5b4d0f1008c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "2c3c54c3-eb86-46c3-98ca-b5b4d0f1008c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c160b207-9e4e-49ac-8e19-b8f286b05bdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}