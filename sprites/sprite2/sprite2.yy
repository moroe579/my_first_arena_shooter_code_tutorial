{
    "id": "1e75233f-b45f-491c-98b4-7852bebc7028",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 46,
    "bbox_right": 71,
    "bbox_top": 43,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fda8dbf6-7381-4a7e-b283-4a8d9f66cfcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e75233f-b45f-491c-98b4-7852bebc7028",
            "compositeImage": {
                "id": "e8c16d67-100b-42c8-8651-9a65c1e3f87b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda8dbf6-7381-4a7e-b283-4a8d9f66cfcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2caebcea-524b-4c66-9f8b-6b777fc13001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda8dbf6-7381-4a7e-b283-4a8d9f66cfcd",
                    "LayerId": "04a3de5b-ba8b-4a54-a1c2-aef4825d437e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "04a3de5b-ba8b-4a54-a1c2-aef4825d437e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e75233f-b45f-491c-98b4-7852bebc7028",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 59,
    "yorig": 57
}