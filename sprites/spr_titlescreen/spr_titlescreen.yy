{
    "id": "dac2beab-2c0e-45c5-8ed2-5b6dcfdd2174",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titlescreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 5,
    "bbox_right": 853,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9499721-6c7f-4e6f-a506-0cc9857ad517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dac2beab-2c0e-45c5-8ed2-5b6dcfdd2174",
            "compositeImage": {
                "id": "5f740c38-4f05-442f-abc1-b9ac15863456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9499721-6c7f-4e6f-a506-0cc9857ad517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29f7d30-c508-42a3-b7e6-38127bb4a46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9499721-6c7f-4e6f-a506-0cc9857ad517",
                    "LayerId": "c56e9d44-edab-47d1-88a7-c938cc1148ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "c56e9d44-edab-47d1-88a7-c938cc1148ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dac2beab-2c0e-45c5-8ed2-5b6dcfdd2174",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}