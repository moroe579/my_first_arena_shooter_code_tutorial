{
    "id": "a6b284c9-15e2-4077-97c8-5bd83f042a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_borderwall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97652c85-9ed2-4cc2-a505-3c705b5b9724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6b284c9-15e2-4077-97c8-5bd83f042a04",
            "compositeImage": {
                "id": "b9992e34-4e1f-4371-b8ac-25dffce62324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97652c85-9ed2-4cc2-a505-3c705b5b9724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a302dbd3-c89e-423e-ab2b-d76daf428f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97652c85-9ed2-4cc2-a505-3c705b5b9724",
                    "LayerId": "76aa92f1-b4c7-4e10-a802-27c23f38dbb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "76aa92f1-b4c7-4e10-a802-27c23f38dbb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6b284c9-15e2-4077-97c8-5bd83f042a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}