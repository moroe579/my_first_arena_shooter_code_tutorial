{
    "id": "b0c57bbd-0ece-480d-ad7f-2533e573c31d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e2a0925-34ab-4171-8e10-a84ea3fb0e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0c57bbd-0ece-480d-ad7f-2533e573c31d",
            "compositeImage": {
                "id": "4fa1cbcf-953a-45b5-8a8e-ff89a3c1a04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e2a0925-34ab-4171-8e10-a84ea3fb0e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4bbaa54-3121-4020-8969-8d44c1689c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e2a0925-34ab-4171-8e10-a84ea3fb0e4e",
                    "LayerId": "3055a367-2973-4957-a932-6f00bdc2d25b"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 64,
    "layers": [
        {
            "id": "3055a367-2973-4957-a932-6f00bdc2d25b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0c57bbd-0ece-480d-ad7f-2533e573c31d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 34,
    "yorig": 32
}