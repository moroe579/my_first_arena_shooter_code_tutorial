{
    "id": "47f43114-92eb-4791-af26-8b43a3a65e69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71ed0442-d520-4aec-a291-8d171191a9ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47f43114-92eb-4791-af26-8b43a3a65e69",
            "compositeImage": {
                "id": "18d88fe5-ca30-4309-ad1d-5a8a4b4d427d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ed0442-d520-4aec-a291-8d171191a9ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb4d889-57ca-460b-a1a8-e48bc9f79e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ed0442-d520-4aec-a291-8d171191a9ad",
                    "LayerId": "36fc19f2-b1f3-49e1-b302-ca46ce9c61cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "36fc19f2-b1f3-49e1-b302-ca46ce9c61cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47f43114-92eb-4791-af26-8b43a3a65e69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}