{
    "id": "85a7b1f6-c244-4392-aff4-152741007680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 13,
    "bbox_right": 103,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "179b9c12-f32a-4888-b12e-9bc8dbdbe274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a7b1f6-c244-4392-aff4-152741007680",
            "compositeImage": {
                "id": "819480ce-347f-4c75-a522-275ac81d875b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179b9c12-f32a-4888-b12e-9bc8dbdbe274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71edacb1-10ba-4acf-b8b5-f18d84e7e535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179b9c12-f32a-4888-b12e-9bc8dbdbe274",
                    "LayerId": "272229c0-c26b-4ee8-b1e0-49b4c58b9132"
                }
            ]
        },
        {
            "id": "530f4da7-9671-4a58-a8d9-290bb096b969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a7b1f6-c244-4392-aff4-152741007680",
            "compositeImage": {
                "id": "fcec32a8-a299-44b0-aee9-99017987f8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "530f4da7-9671-4a58-a8d9-290bb096b969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e48eda-8d2c-46f0-9fa3-5a1e219e284d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "530f4da7-9671-4a58-a8d9-290bb096b969",
                    "LayerId": "272229c0-c26b-4ee8-b1e0-49b4c58b9132"
                }
            ]
        },
        {
            "id": "79bb17e9-8eb3-498b-b8e3-19a6d95a218c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a7b1f6-c244-4392-aff4-152741007680",
            "compositeImage": {
                "id": "bab1662f-5b89-460d-b6de-28b7cf00211f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bb17e9-8eb3-498b-b8e3-19a6d95a218c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bc5a0d-4731-4504-bc0f-0cce46575c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bb17e9-8eb3-498b-b8e3-19a6d95a218c",
                    "LayerId": "272229c0-c26b-4ee8-b1e0-49b4c58b9132"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "272229c0-c26b-4ee8-b1e0-49b4c58b9132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85a7b1f6-c244-4392-aff4-152741007680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}