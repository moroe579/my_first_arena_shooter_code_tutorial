{
    "id": "8dc2ce79-2f08-43ef-92fd-74fda8fa6ac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 22,
    "bbox_right": 91,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac82afc6-c3d7-4fad-9208-556cd87b0bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dc2ce79-2f08-43ef-92fd-74fda8fa6ac2",
            "compositeImage": {
                "id": "4051f279-afae-4ea5-8c85-86ffdbd17943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac82afc6-c3d7-4fad-9208-556cd87b0bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564da2f8-7686-4fbd-b63e-de7cea67dc8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac82afc6-c3d7-4fad-9208-556cd87b0bff",
                    "LayerId": "8e932975-e6b6-4f90-8af1-bb6758565b48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "8e932975-e6b6-4f90-8af1-bb6758565b48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dc2ce79-2f08-43ef-92fd-74fda8fa6ac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}