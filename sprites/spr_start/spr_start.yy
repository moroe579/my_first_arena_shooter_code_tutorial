{
    "id": "6d8ad1d2-f9a2-4115-8073-667eaa8bb768",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9cf4eec-dad0-457a-8f98-421204a76069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8ad1d2-f9a2-4115-8073-667eaa8bb768",
            "compositeImage": {
                "id": "1fe0f11f-972c-4b9a-a7da-ad4015b0fb87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cf4eec-dad0-457a-8f98-421204a76069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3cc5e61-6e78-4412-aef8-54720f46cdd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cf4eec-dad0-457a-8f98-421204a76069",
                    "LayerId": "3cfa7546-947c-4c12-bab8-c4b2fba55725"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3cfa7546-947c-4c12-bab8-c4b2fba55725",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d8ad1d2-f9a2-4115-8073-667eaa8bb768",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}