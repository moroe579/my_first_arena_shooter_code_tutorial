{
    "id": "8735e6a3-bac4-41cc-afec-685add6961ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_quit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6c1adbc-5e26-46b6-beda-19f539f59ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8735e6a3-bac4-41cc-afec-685add6961ed",
            "compositeImage": {
                "id": "81fd98bd-5716-4a32-b1bb-e2095f107fe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c1adbc-5e26-46b6-beda-19f539f59ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a36969-f4d0-4bb5-abec-fe1b1d2f539c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c1adbc-5e26-46b6-beda-19f539f59ee1",
                    "LayerId": "59d46ba2-c60e-4f42-82bb-9c138e0f44af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "59d46ba2-c60e-4f42-82bb-9c138e0f44af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8735e6a3-bac4-41cc-afec-685add6961ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}