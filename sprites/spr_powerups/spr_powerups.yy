{
    "id": "539e9093-a92c-4da0-bbb9-3b3a43e79876",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powerups",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f2eb42e-da30-4320-a1d3-02dfe97eeded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "539e9093-a92c-4da0-bbb9-3b3a43e79876",
            "compositeImage": {
                "id": "928337f6-4857-4b14-b550-5edcec5d7f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2eb42e-da30-4320-a1d3-02dfe97eeded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6600793e-6ee2-4251-84a2-e937053201e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2eb42e-da30-4320-a1d3-02dfe97eeded",
                    "LayerId": "ea311e10-16cf-4579-8fff-624f9e39cbc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "ea311e10-16cf-4579-8fff-624f9e39cbc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "539e9093-a92c-4da0-bbb9-3b3a43e79876",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 15
}