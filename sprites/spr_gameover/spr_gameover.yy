{
    "id": "4ca07fb3-0374-403b-bed6-2c293c6f4fb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 133,
    "bbox_left": 30,
    "bbox_right": 230,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a736e224-6a56-4023-bb96-0bd4ca64f43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ca07fb3-0374-403b-bed6-2c293c6f4fb2",
            "compositeImage": {
                "id": "f97deb30-7dbd-4eb6-b9a8-a714b52485ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a736e224-6a56-4023-bb96-0bd4ca64f43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454c5f2b-6bf0-4b47-a74a-df3240f7a697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a736e224-6a56-4023-bb96-0bd4ca64f43e",
                    "LayerId": "d266a864-fdf3-47f2-959f-2a63920e9861"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d266a864-fdf3-47f2-959f-2a63920e9861",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ca07fb3-0374-403b-bed6-2c293c6f4fb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}