{
    "id": "8207f78b-13fe-4ff0-9df8-12dd8818e2db",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "aabbc167-4033-4b99-95b8-9a3449531683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bf90bd11-1c80-42f1-9788-f4dbbbd2a822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 185,
                "y": 96
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ff537a78-906e-4cac-9d51-33e8d662df2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 173,
                "y": 96
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2273c6cc-061e-4ab2-8050-7d6ff4504aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 144,
                "y": 96
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e60dc4db-52ae-4d6f-9184-4322dc40328d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 123,
                "y": 96
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e8d569d9-e27c-442b-b1eb-9759378b0632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 97,
                "y": 96
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "39767ce0-b250-4e4e-96c6-2a5c3405438d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 76,
                "y": 96
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c5d61567-84e7-4960-add4-4403ce6df09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 70,
                "y": 96
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "da6cef31-7116-41e8-ad85-1d7a5b5db83d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 58,
                "y": 96
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c2e90579-2100-4f1c-ba7b-b25fb13581ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 46,
                "y": 96
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f7be4385-97fb-457d-8396-5e085bfab12f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 192,
                "y": 96
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6632ab0c-2982-4442-b012-6eb7d46c5cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 29,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b48384b0-2a90-48bc-b434-27c58b90c20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a8a9f8bb-4db4-4359-a8ca-d0e9444f15b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 496,
                "y": 49
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "adca2468-99dd-4d0d-a7a1-ee08795ac03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 489,
                "y": 49
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a051782d-ea52-4c13-b8e2-f839dab8452c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 472,
                "y": 49
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "334a532c-d090-404e-8e09-c0e82093ab58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 451,
                "y": 49
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "77d41a85-fef2-41ef-a042-b6b3fbee62f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 438,
                "y": 49
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "19974b6e-d314-428e-837e-ddfd217df689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 420,
                "y": 49
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "411c524d-f136-4b50-b704-90407e1cf090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 403,
                "y": 49
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2dc280eb-bf8d-4223-ba45-4b2fe3137e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 382,
                "y": 49
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "45d85de0-c548-40c2-aa30-1f29a36648d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 9,
                "y": 96
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2b20c4ff-f62c-4ebe-b0ae-73d13ec73e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 210,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "eda6bab6-9a36-48b6-89ae-2422f1700946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 229,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "182a3488-af7b-4347-a870-d662baedaa25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 249,
                "y": 96
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7ab87196-94aa-4ed5-adb4-cff6bcfeeacf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 146,
                "y": 143
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "719641df-9fef-400b-ade5-d11cd2372cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 139,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "795ddbf8-0397-46ac-9a05-4cde443c16b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 131,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "74797525-3127-4d18-b176-02e1d39ee1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 119,
                "y": 143
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c8cae59e-e4bc-44ba-b495-64cb73b464c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 104,
                "y": 143
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8d71d921-a7d7-477b-8a99-71d9e4d9c00d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 90,
                "y": 143
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ecf5a3d4-9b4a-4352-822e-3f9ffa66606a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 72,
                "y": 143
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1d0dd44d-601b-4d15-8228-1cd47e3a2482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 43,
                "y": 143
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3385aaf5-def9-4669-a812-a834a1bb930d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 21,
                "y": 143
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "264afac8-f58c-4780-965b-5919f34f6a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "590555f8-5f2f-475b-a38b-8cc45a3a10af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 481,
                "y": 96
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "204c8185-6659-4bcd-8e47-c4c9e0231979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 459,
                "y": 96
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e8f13f6c-11b4-4a98-94fe-33538d89838c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 440,
                "y": 96
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1ac071f7-65b0-4599-9b33-0938fa7df8cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 421,
                "y": 96
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2b5ad44f-7fe3-45bf-a595-2e31653714dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 398,
                "y": 96
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c2c6f326-9405-4b10-ba96-ebfc649cc12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 375,
                "y": 96
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "69fdd239-a270-4b2c-978c-b765261f9b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 357,
                "y": 96
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "eb4b4118-9051-48f3-b071-aef50f495dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 335,
                "y": 96
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e1593f51-37a2-4480-b676-43e476fa19e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 316,
                "y": 96
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6dd7b603-7b8c-41d8-b702-c19cd5af56df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 297,
                "y": 96
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4277af3c-3c90-4a12-a818-699d5be23dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 268,
                "y": 96
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "52f7509c-456c-4e91-b7e4-894ae3f77d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 356,
                "y": 49
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b30fa98a-1191-47d0-965d-c28455a27c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 330,
                "y": 49
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c1d6c685-15b8-40fa-8ddb-47aacd3c17b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 313,
                "y": 49
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5e7d6416-58a8-4a7b-a053-9380824bacf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8d62e99b-3765-40ae-9ae9-bc617b33907d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3d2e4192-9c81-4a3d-b5f9-59064ef3e6a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1dbfe93a-4e86-4455-be59-c73fc5122499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "86b38da5-a425-40f7-a1e2-2b4afa1ba390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5b80fa9c-e791-4d6d-a6f9-54dab36c2ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "90870d46-ba58-4a25-896e-48502fa2ed47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5940fbc7-f634-4937-b9e3-f275bd1546aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0041bb48-89a9-4897-ad0e-4c63cf0eddf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "65d2e387-f9fd-41c1-b660-80bf28697359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a03df68a-8b9c-4ff1-b36e-e5150a7bb53d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4d03bf63-02ab-4a83-b861-d9baf885dde6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "dd8efc1d-b2d1-4611-8084-320bfc5c1bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "53b53eba-cbbd-45cf-a611-05556eb89aee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "698b3491-6066-4dc3-9f0f-a97bccef2902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b33fbd0e-28bd-4229-890a-ddbf0bc08a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "53cda3b3-ca93-4b11-911b-9aea33d12784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "27075283-cba7-4072-9185-b008290ecde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5cb960cf-023e-4823-aeed-f387661d4f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dc63a68d-5724-489f-9c5c-2feffba1635b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cb8b5510-2b19-4b29-b442-81d6243ff62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "69e8b746-5610-46f7-8de1-064483fcac0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f9c65d57-d61c-4900-9744-e0ae2848cbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 446,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4feba96d-27b4-40f3-bfd3-bc953a27eff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 117,
                "y": 49
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "45b0e54e-98dc-4f2b-9979-bf53ab60a1fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "43aa4b7b-3adb-46c5-8ab0-e51e427b773e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -1,
                "shift": 13,
                "w": 12,
                "x": 282,
                "y": 49
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0081e32d-05b4-4876-8c8f-7ff59cd02a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 265,
                "y": 49
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "62814008-d89b-466f-8d6e-3c05a4202c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 258,
                "y": 49
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a4ada814-3118-462b-8849-285a4bc776cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 233,
                "y": 49
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5b0c30e3-b4ba-4080-bfb4-dc7b31908427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 49
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9fea1ccb-3037-4827-a7b5-01aaf61f1c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 199,
                "y": 49
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "49ebb218-5e95-403b-a4b9-1d5eb43ed3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 182,
                "y": 49
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cd348191-efc9-40e4-a156-6dcd7bae974f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 165,
                "y": 49
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3806b68a-edf3-4707-89eb-73f053f237f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 150,
                "y": 49
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6a9851bf-5e84-4df3-8aa2-eb35e52004ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 296,
                "y": 49
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2a79fc7e-5d80-41eb-b962-754a390855fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 134,
                "y": 49
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "43d8f0b3-2969-4851-9644-2edf01f29d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 100,
                "y": 49
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d94981e1-0489-4c4c-8850-d75498089f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 82,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8a4b04cf-919e-4cb3-b192-950d16dfe3dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 59,
                "y": 49
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "947b85f5-4594-471b-83bd-1011a239ec43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 39,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b81455c6-ce16-492b-a7e2-ef95856fe314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 20,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "93d51fe3-c35e-4a5b-9e59-a4842e2313ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "41c1bb8b-9b95-4068-a937-6d3263821a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "807a142d-d045-4c81-8dfb-5f475051bb3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 484,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "88b62c1b-5e1b-470f-838a-0f62dbb7f183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "22d2e0ca-6fb1-4646-a08f-e924450ba81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 166,
                "y": 143
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "fb0b7e86-b2ac-4115-934e-83e133c6972e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 45,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 185,
                "y": 143
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Score: 123456",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}