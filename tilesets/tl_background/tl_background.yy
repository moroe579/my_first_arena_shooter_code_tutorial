{
    "id": "b50d3747-7696-43df-9f68-2f71cba6d265",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tl_background",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 3,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "47f43114-92eb-4791-af26-8b43a3a65e69",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 12,
    "tileheight": 128,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 128,
    "tilexoff": 0,
    "tileyoff": 0
}